﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotator : MonoBehaviour
{
    public float tumble;

    void Start() {
        Rigidbody rigidbody = GetComponent<Rigidbody>();
        // return a random point inside a sphere with radius 1
        // x, y, z will be randomized individually
        rigidbody.angularVelocity = Random.insideUnitSphere * tumble;
    }
}
