﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour
{
    public GameObject explosion;
    public GameObject playerExplosion;
    public int scoreValue;
    private GameController gameController;

    void Start() {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null) {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null) {
            Debug.Log("Cannot find 'CameController' script");
        }
    }

   void OnTriggerEnter(Collider other) {
       if (other.CompareTag("Boundary") || other.CompareTag("Enemy")) {
           return;
       }
       if (explosion != null) {
           // instantiate in exact same position as the asteroid or enemy
           Instantiate (explosion, transform.position, transform.rotation);
       }
        if (other.tag == "Player") {
            Instantiate (playerExplosion, other.transform.position, other.transform.rotation);
            gameController.GameOver();
        }
       gameController.AddScore(scoreValue);
       // marked to be destroyed and destroyed when frame over
       Destroy(other.gameObject); //destroy bolt
       Destroy(gameObject); // destroy the asteroid
   }
}
